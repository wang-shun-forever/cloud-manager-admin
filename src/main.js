import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
// 引入所有样式

import Antd, { Icon } from "ant-design-vue"; // 引入antd
import "./styles/antd-variables.less";

import "element-ui/lib/theme-chalk/index.css";
import ElementUI from "element-ui";
import "./styles/index.less"; // 引入样式

import i18n from "./lang"; // i18n
import Components from "@/components"; // 全局组件的注册

// icon
import "@/icons";

Vue.use(Antd);
// 全局注册element-ui
Vue.use(ElementUI);
// 全局组件注册
Vue.use(Components);

Vue.config.productionTip = false;
// permission
import "@/permission"; // permission control

const MyIcon = Icon.createFromIconfontCN({
  scriptUrl: '//at.alicdn.com/t/font_2749943_9mizgiys7ta.js', // 在 iconfont.cn 上生成
});

Vue.component('my-icon', MyIcon)

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount("#app");
