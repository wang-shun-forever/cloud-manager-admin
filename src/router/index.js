/**
 * Created by wangshun 21/12/15
 */
import Vue from "vue";
import VueRouter from "vue-router";
// 引入多模块规则
import role from "@/router/modules/role";
// import user from "@/router/modules/user";
import userGroup from "@/router/modules/userGroup";

Vue.use(VueRouter);

import Layout from "@/layout";

// 动态路由
export const asyncRoutes = [userGroup, role];

// 静态路由
export const constantRoutes = [
  {
    name: "user", // 后续做权限的时候会用
    path: "/",
    redirect: "/user",
    component: Layout,
    children: [
      {
        path: "user", // 二级路由的默认路由
        component: () => import("@/views/user/user"),
        meta: {
          title: "用户管理",
          icon: "dashboard"
        }
      }
    ]
  },
  {
    path: "/login",
    name: "Login",
    component: () => import("@/views/login/index"),
    hidden: true
  },
  {
    name: "modelTask",
    path: "/ModelTask",
    component: Layout,
    children: [
      {
        path: "", // 二级路由的默认路由
        component: () => import("@/views/modelTask/modelTask"),
        meta: {
          title: "模型库管理",
          icon: "user"
        }
      }
    ]
  },
  {
    name: "dataSource",
    path: "/DataSource",
    component: Layout,
    children: [
      {
        path: "", // 二级路由的默认路由
        component: () => import("@/views/dataSource/dataSource"),
        meta: {
          title: "数据源管理",
          icon: "user"
        }
      }
    ]
  },
  // 404 page must be placed at the end !!!
  { path: "*", redirect: "/404", hidden: true }
];

// 解决面包屑重复点击路由导航报错问题
const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err);
};

const createRouter = () =>
  new VueRouter({
    // mode: 'history', // require service support
    scrollBehavior: () => ({ y: 0 }),
    // routes: constantRoutes
    // 合并静态路由和动态路由
    routes: [...asyncRoutes, ...constantRoutes]
  });

const router = createRouter();

export function resetRouter() {
  const newRouter = createRouter();
  router.matcher = newRouter.matcher; // reset router
}

export default router;
