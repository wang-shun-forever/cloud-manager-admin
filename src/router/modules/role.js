import Layout from "@/layout";
// 导出角色的路由规则
export default {
  name: "role",
  path: "/role",
  component: Layout,
  children: [
    {
      path: "", // 二级路由的默认路由
      component: () => import("@/views/role/role"),
      meta: {
        title: "角色管理",
        icon: "user"
      }
    }
  ]
};
