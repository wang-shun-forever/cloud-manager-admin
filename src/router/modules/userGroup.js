import Layout from "@/layout";
// 导出用户的路由规则
export default {
  name: "userGroup", // 后续做权限的时候会用
  path: "/usergroup",
  component: Layout,
  // redirect: "/usergroup",
  children: [
    {
      path: "", // 二级路由的默认路由
      component: () => import("@/views/userGroup/userGroup"),
      meta: {
        title: "用户组管理",
        icon: "form"
      }
    }
  ]
};
