import Layout from "@/layout";
// 导出用户的路由规则
export default {
  name: "user", // 后续做权限的时候会用
  path: "/",
  redirect: "/user",
  component: Layout,
  children: [
    {
      path: "user", // 二级路由的默认路由
      component: () => import("@/views/user/user"),
      meta: {
        title: "用户管理",
        icon: "dashboard"
      }
    }
  ]
};
