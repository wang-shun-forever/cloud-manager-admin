/**
 * Created by wangshun 21/12/15
 */
import { getToken, setToken, removeToken } from "@/utils/auth";
import { login } from "@/api/user";
const state = {
  token: getToken(),
  username: undefined
};
const mutations = {
  setToken(state, token) {
    state.token = token;
    setToken(token);
  },
  removeToken(state) {
    state.token = null;
    removeToken();
  },
  setUserInfo(state, result) {
    state.username = result;
  },
  removeUserInfo(state) {
    state.username = undefined;
  }
};
const actions = {
  // 登陆
  async login(context, reqdata) {
    // action 里面请求的接口
    const { authorization, data } = await login(reqdata);
    // 失败的情况已经在响应拦截器中处理过了
    context.commit("setToken", authorization);
    // 用户个人信息在登陆接口中
    context.commit("setUserInfo", data.descr);
    console.log("设置token完成");
    console.log(data.descr);
    return data;
  },
  // 登出
  logout(context) {
    // 删除 token
    context.commit("removeToken");
    // 删除用户资料
    context.commit("removeUserInfo");
  }
};
export default {
  namespaced: true,
  state,
  mutations,
  actions
};
