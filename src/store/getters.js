/**
 * Created by wangshun 21/12/14
 */
const getters = {
  token: state => state.user.token,
  username: state => state.user.username
};
export default getters;
