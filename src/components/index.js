/**
 * Created by wangshun 21/12/16
 */

/* 全局组件注册 */

import PageTools from "./PageTools";

export default {
  install(Vue) {
    Vue.component("PageTools", PageTools);
  }
};
