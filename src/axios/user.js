import axios from 'axios'
import {getManagerUrl} from './ip'
// 获取用户列表
export function getUserList () {
  let managerIp = getManagerUrl()
  return axios.get(managerIp + '/clouddisk/rest/users')
}
// 搜索
export function searchUser (keyword) {
  let managerIp = getManagerUrl()
  return axios.get(managerIp + '/clouddisk/rest/users?keywords=' + keyword)
}
// 创建用户
export function createUser (username, password) {
  let managerIp = getManagerUrl()
  return axios.post(managerIp + '/clouddisk/rest/users/signup', {
    username: username,
    password: password
  })
}
// 删除用户
export function deleteUser (id) {
  let managerIp = getManagerUrl()
  return axios.delete(managerIp + '/clouddisk/rest/users?id=' + id)
}
// 更新用户信息
export function renewInfo (id, user) {
  let managerIp = getManagerUrl()
  return axios.put(managerIp + '/clouddisk/rest/users?id=' + id, user)
}
// 更新用户密码
export function renewPwd (id, oldPassword, newPassword) {
  let managerIp = getManagerUrl()
  return axios.put(managerIp + '/clouddisk/rest/users/pwd?id=' + id + '&oldPassword=' + oldPassword + '&newPassword=' + newPassword)
}
// 设置用户所属角色
export function setUserRole (userId, roleId) {
  let managerIp = getManagerUrl()
  return axios.put(managerIp + '/clouddisk/rest/users/role?userId=' + userId + '&roleId=' + roleId)
}
// 获取用户所有角色
export function getAllRole (userId) {
  let managerIp = getManagerUrl()
  return axios.get(managerIp + '/clouddisk/rest/users/role?userId=' + userId)
}
// 设置用户所属用户组及角色
export function set (userId, array) {
  let managerIp = getManagerUrl()
  return axios.put(managerIp + '/clouddisk/rest/users/userGroups?userId=' + userId, array)
}
