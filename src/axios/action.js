import axios from 'axios'
import {getManagerUrl} from './ip'

// 获取基本行为统计值
export function getActionstate (beginTime, endTime) {
  let managerIp = getManagerUrl()
  const data = {
    beginTime,
    endTime,
    pageSize: 100,
    pageNum: 1
  }
  return axios.get(managerIp + '/clouddisk/rest/audit/stat', {params: data})
}

// 获取模型执行统计值
export function getModelTaskstate (beginTime, endTime) {
  let managerIp = getManagerUrl()
  const data = {
    beginTime,
    endTime,
    pageSize: 500,
    pageNum: 1
  }
  return axios.get(managerIp + '/clouddisk/rest/audit/stat/model', {params: data})
}
