import axios from 'axios'
import {getManagerUrl} from './ip'

// 获取底层模型列表（igs、bigdata下的）
export function getModelDataList (type, id) {
  let managerIp = getManagerUrl()
  if (!id) {
    return axios.get(managerIp + '/clouddisk/rest/CalculateModel/catalog/' + type)
  } else {
    return axios.get(managerIp + '/clouddisk/rest/CalculateModel/catalog/' + type + '?modelID=' + id)
  }
}

// 获取模型目录树（递归形式呈现）
export function getModelCatalogTree () {
  let managerIp = getManagerUrl()
  return axios.get(managerIp + '/clouddisk/rest/CalculateModel/nodes')
}

// 获取根节点，若没有则会自动创建
export function getTreeRootNode () {
  let managerIp = getManagerUrl()
  return axios.get(managerIp + '/clouddisk/rest/CalculateModel/node/root')
}

// 创建模型目录节点
export function createModelCatalog (ModelCatalog) {
  let managerIp = getManagerUrl()
  return axios.post(managerIp + '/clouddisk/rest/CalculateModel/node', ModelCatalog)
}

// 删除模型目录节点
export function deleteModelCatalog (id) {
  let managerIp = getManagerUrl()
  return axios.delete(managerIp + '/clouddisk/rest/CalculateModel/node/' + id)
}

// 更新模型目录节点
export function renewModelCatalog (modelCatalog) {
  let managerIp = getManagerUrl()
  return axios.put(managerIp + '/clouddisk/rest/CalculateModel/node', modelCatalog)
}

// 获取节点下的模型
export function getModelByNode (id, keyword) {
  let managerIp = getManagerUrl()
  return axios.get(managerIp + '/clouddisk/rest/CalculateModel/catalog?nodeId=' + id + '&keyword=' + keyword)
}

// 创建节点下的模型
export function createModelByNode (id, modelInfo) {
  let managerIp = getManagerUrl()
  return axios.post(managerIp + '/clouddisk/rest/CalculateModel?nodeId=' + id, modelInfo)
}

// 删除节点下的模型
export function deleteModelByNode (id) {
  let managerIp = getManagerUrl()
  return axios.delete(managerIp + '/clouddisk/rest/CalculateModel/' + id)
}

// 更新节点下的模型
export function renewModelByNode (model) {
  let managerIp = getManagerUrl()
  return axios.put(managerIp + '/clouddisk/rest/CalculateModel', model)
}

// 上传小文件（图片）
export function uploadImg (imgFile) {
  let managerIp = getManagerUrl()
  return axios({
    method: 'post',
    data: imgFile,
    headers: {
      'Content-Type': 'multipart/form-data'
    },
    url: managerIp + '/clouddisk/rest/file/upload'
  })
  // return axios.post(managerIp + '/clouddisk/rest/file/upload?folderDir=default', imgFile)
}
