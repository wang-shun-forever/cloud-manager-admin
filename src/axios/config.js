import axios from 'axios'
import {getManagerUrl} from './ip'

// 获取配置文件
export function getConfigFile (fileName) {
  let managerIp = getManagerUrl()
  return axios.get(managerIp + '/clouddisk/rest/tools/config/' + fileName)
}

// 更新配置文件
export function setConfigFile (config) {
  let managerIp = getManagerUrl()
  return axios.put(managerIp + '/clouddisk/rest/tools/config', config)
}
