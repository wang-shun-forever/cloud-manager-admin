import axios from 'axios'
import {getManagerUrl} from './ip'
// 获取角色列表
export function getRoleList () {
  let mangerIp = getManagerUrl()
  return axios.get(mangerIp + '/clouddisk/rest/role')
}

// 添加角色
export function addRole (name, descpt, roleType) {
  let mangerIp = getManagerUrl()
  return axios.post(mangerIp + '/clouddisk/rest/role?name=' + name + '&descpt=' + descpt + '&roleType=' + roleType)
}

// 修改指定角色
export function modifyRole (roleid, newName, newDescpt) {
  let mangerIp = getManagerUrl()
  return axios.put(mangerIp + '/clouddisk/rest/role?roleId=' + roleid + '&newName=' + newName + '&newDescpt=' + newDescpt)
}

// 删除角色
export function deleteRole (roleids) {
  let mangerIp = getManagerUrl()
  return axios.delete(mangerIp + '/clouddisk/rest/role?roleIds=' + roleids)
}
