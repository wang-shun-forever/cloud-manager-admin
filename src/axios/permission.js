import axios from 'axios'
import {getManagerUrl} from './ip'

// 获取系统功能权限列表
export function getPermissinonList () {
  let managerIp = getManagerUrl()
  return axios.get(managerIp + '/clouddisk/rest/function')
}

// 获取角色的权限信息
export function getRolePermissionInfo (roleid) {
  let managerIp = getManagerUrl()
  return axios.get(managerIp + '/clouddisk/rest/permission?roleId=' + roleid)
}

// 设置角色的权限信息
export function setRolePermissionInfo (roleid, array) {
  let managerIp = getManagerUrl()
  return axios.put(managerIp + '/clouddisk/rest/permission?roleId=' + roleid, array)
}
