import axios from 'axios'
import {getManagerUrl} from './ip'

// 获取数据源类型
export function getDataSourcesType (dstype) {
  let managerIp = getManagerUrl()
  return axios.get(managerIp + '/giscore/dataconvert/rest/datasources/types?dstype=' + dstype)
}

// 获取数据库数据源列表
export function getDataSources (dstype) {
  let managerIp = getManagerUrl()
  return axios.get(managerIp + '/giscore/dataconvert/rest/datasources?dstype=' + dstype)
}

// 获取数据库数据源(按id)
export function getDataSourceById (id) {
  let managerIp = getManagerUrl()
  return axios.get(managerIp + '/giscore/dataconvert/rest/datasources/' + id)
}

// 数据源连接测试
export function checkDSConnection (dataSource) {
  let managerIp = getManagerUrl()
  return axios.post(managerIp + '/giscore/dataconvert/rest/datasources/connetion', dataSource)
}

// 新增数据源
export function createDataSource (dataSource) {
  let managerIp = getManagerUrl()
  return axios.post(managerIp + '/giscore/dataconvert/rest/datasources', dataSource)
}

// 更新数据源
export function renewDataSource (dataSource) {
  let managerIp = getManagerUrl()
  return axios.put(managerIp + '/giscore/dataconvert/rest/datasources', dataSource)
}

// 初始化数据源
export function initDataSources () {
  let managerIp = getManagerUrl()
  return axios.post(managerIp + '/giscore/dataconvert/rest/datasources/init')
}

// 删除数据源
export function deleteDataSource (id) {
  let managerIp = getManagerUrl()
  return axios.delete(managerIp + '/giscore/dataconvert/rest/datasources/' + id)
}
