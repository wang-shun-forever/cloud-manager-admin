import axios from 'axios'
import {getManagerUrl} from './ip'

// 获取用户组织树结构接口
export function userGroupTree (userId) {
  let mangerIp = getManagerUrl()
  return axios.get(mangerIp + '/clouddisk/rest/userGroups?includeChilds=true', {
    userId: userId
  })
}
export function userGroupTreeByUser (userId) {
  let mangerIp = getManagerUrl()
  return axios.get(mangerIp + '/clouddisk/rest/userGroups?includeChilds=true' + '&userId=' + userId)
}
// 创建组织结构节点
export function createUserGroup (name, pGroupId, descr) {
  let mangerIp = getManagerUrl()
  return axios.post(mangerIp + '/clouddisk/rest/userGroups', {
    name: name,
    descr: descr,
    pGroupId: pGroupId
  })
}
// 删除组织节点结构
export function deleteUserGroup (groupId) {
  let mangerIp = getManagerUrl()
  return axios.delete(mangerIp + '/clouddisk/rest/userGroups?id=' + groupId)
}
// 更新组织结构节点
export function renewUserGroup (id, userGroup) {
  let mangerIp = getManagerUrl()
  return axios.put(mangerIp + '/clouddisk/rest/userGroups?id=' + id, userGroup)
}
// 获取指定组织结构节点下用户列表
export function getUserListFromUserGroup (groupId) {
  let mangerIp = getManagerUrl()
  // 要求必须带token，本地可任意填写，要考虑真实第三方环境怎么解决？
  return axios.get(mangerIp + '/clouddisk/rest/userGroups/users?groupId=' + groupId + '&token=qwer')
}
// 向指定组织结构节点添加用户
export function addUser (groupId, userIds) {
  let mangerIp = getManagerUrl()
  return axios.post(mangerIp + '/clouddisk/rest/userGroups', {
    groupId: groupId,
    userIds: userIds
  })
}
// 从指定组织结构节点移除用户
export function removeUser (groupId, userIds) {
  let mangerIp = getManagerUrl()
  return axios.delete(mangerIp + '/clouddisk/rest/userGroups/users', {
    groupId: groupId,
    userIds: userIds
  })
}
