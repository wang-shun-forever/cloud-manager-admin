/**
 * Created by wangshun 21/12/14
 */
import request from "@/utils/request";

// 用户登陆
export function login(data) {
  return request({
    url: "/users/login",
    method: "POST",
    data
  });
}

// 获取用户列表
export function getUserList() {
  return request({
    url: "/users",
    method: "GET"
  });
}

// TODO
// 搜索
export function searchUser(keyword) {
  return request({
    url: "/users",
    method: "GET",
    params: {
      keywords: keyword
    }
  });
  // let managerIp = getManagerUrl()
  // return axios.get(managerIp + '/clouddisk/rest/users?keywords=' + keyword)
}

// 创建用户
export function createUser(username, password) {
  return request({
    url: "/users/signup",
    method: "POST",
    data: {
      username: username,
      password: password
    }
  });
}

// 更新用户信息
export function renewInfo(id, data) {
  return request({
    url: `/users?id=${id}`,
    method: "PUT",
    data
  });
}

// 删除用户
export function deleteUser(id) {
  return request({
    url: `/users?id=${id}`,
    method: "DELETE"
  });
}

// 更新用户密码
export function renewPwd(id, oldPassword, newPassword) {
  return request({
    url: `/users/pwd?id=${id}&oldPassword=${oldPassword}&newPassword=${newPassword}`,
    method: "PUT"
  });
}

// 设置用户所属角色
export function setUserRole(userId, roleId) {
  return request({
    url: `users/role?userId=${userId}&roleId=${roleId}`,
    method: "PUT"
  });
}
export function getInfo() {}

export function logout() {}
