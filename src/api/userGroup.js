/**
 * Created by wangshun 21/12/20
 */
import request from "@/utils/request";

// 获取用户组织树结构接口
export function userGroupTree(userId) {
  return request({
    url: "userGroups?includeChilds=true",
    method: "GET",
    params: {
      userId
    }
  });
}

// 创建组织结构节点
export function createUserGroup(name, pGroupId, descr) {
  return request({
    url: "userGroups",
    method: "POST",
    data: {
      name,
      descr,
      pGroupId
    }
  });
}

// 删除组织节点结构
export function deleteUserGroup(groupId) {
  console.log(groupId);
  return request({
    url: `/userGroups?id=${groupId}&includeChilds=${true}`,
    method: "DELETE"
  });
}

// 更新组织结构节点
export function renewUserGroup(id, userGroup) {
  return request({
    url: `userGroups?id=${id}`,
    method: "PUT",
    data: userGroup
  });
  let mangerIp = getManagerUrl();
  return axios.put(mangerIp + "/clouddisk/rest/userGroups?id=" + id, userGroup);
}
