/**
 * Created by wangshun 21/12/14
 */
import request from "@/utils/request";
import { method } from "bluebird";

// 获取角色列表
export function getRoleList() {
  return request({
    url: "/role",
    method: "GET"
  });
}

// 添加角色
export function addRole(name, descpt, roleType) {
  return request({
    url: `role?name=${name}&descpt=${descpt}&roleType=${roleType}`,
    method: "POST"
  });
}

// 删除角色
export function deleteRole(roleids) {
  return request({
    url: `role?roleIds=${roleids}`,
    method: "DELETE"
  });
}

// 修改指定角色
export function modifyRole(roleid, newName, newDescpt) {
  return request({
    url: `role?roleId=${roleid}&newName=${newName}&newDescpt=${newDescpt}`,
    method: "PUT"
  });
}

// 获取角色的权限信息
export function getRolePermissionInfo(roleid) {
  return request({
    url: `permission?roleId=${roleid}`,
    method: "GET"
  });
}

// 设置角色的权限信息
export function setRolePermissionInfo(roleid, array) {
  return request({
    url: `permission?roleId=${roleid}`,
    method: "PUT",
    data: array
  });
}
