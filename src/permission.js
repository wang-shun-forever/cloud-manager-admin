/**
 * Created by wangshun 21/12/14
 */
import router from "@/router";
import store from "@/store";
import nprogress from "nprogress";
import "nprogress/nprogress.css";

// 设置白名单
const whiteList = ["/login", "/404"];

router.beforeEach((to, from, next) => {
  // 开启进度条
  nprogress.start();
  if (store.getters.token) {
    if (to.path === "/login") {
      next("/");
    } else {
      next();
    }
  } else {
    // 没有token
    if (whiteList.includes(to.path)) {
      next();
    } else {
      next("/login");
    }
  }

  // 结束进度条
  nprogress.done();
});

// 后置守卫
router.afterEach(() => {
  // 完成进度条
  nprogress.done();
});
