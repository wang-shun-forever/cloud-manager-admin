/**
 * Created by wangshun 21/12/14
 */
import axios from "axios";
import { message } from "ant-design-vue";
import store from "@/store";
import { getTimeStamp } from "./auth";
import router from "@/router";

// 定义token时效
// const TIMEOUT = 36000;

const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API,
  timeout: 5000
});

// 处理axios请求拦截
service.interceptors.request.use(
  config => {
    // 存在token
    if (store.getters.token) {
      // if (!isCheckTimeOut()) {
      //   // token 过期
      //   store.dispatch("user/logout");
      //   router.push("/login");
      // }
      config.headers["Authorization"] = `Bearer ${store.getters.token}`;
    }
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);

// 处理axios响应式拦截
service.interceptors.response.use(
  function(response) {
    // axios 默认加了一个data
    console.log("utils-request响应拦截器开始拦截");
    console.log("response", response);
    const { status, msg, data } = response.data;
    const { authorization } = response.headers;
    if (status || status === 0) {
      if (status === 0) {
        console.log("status======0");
        return response;
      }
      if (authorization) {
        const userInfo = {};
        userInfo.authorization = authorization;
        userInfo.data = data;
        return userInfo;
      }
      return data;
    } else {
      // 业务逻辑有问题
      console.log("utils-request响应拦截器发现业务逻辑错误");
      message.error(msg);
      return Promise.reject(new Error(msg));
    }
  },
  function(error) {
    message.error(error.message);
    return Promise.reject(error);
  }
);

// 检查token过期时效
function isCheckTimeOut() {
  // 当前时间-缓存的时间 是否大于 1 小时
  const currentTime = Date.now();
  const timeStamp = getTimeStamp();
  return (currentTime - timeStamp) / 1000 > TIMEOUT;
}

export default service;
