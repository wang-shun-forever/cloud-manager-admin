/**
 * Created by wangshun on 21/12/14
 * 自定义验证规则
 */

/**
 * @param {string} path
 * @returns {Boolean}
 */
export function isExternal(path) {
  return /^(https?:|mailto:|tel:)/.test(path);
}

/**
 * @param {string} str
 * @returns {Boolean}
 */
export function validMobile(str) {
  return /^1[3-9]\d{9}$/.test(str);
}
